# Arch Basic Installs

## Basic steps

```
sudo su

pacman -Syu cairo-dock
pacman -Syu chromium 
pacman -Syu dbeaver 
pacman -Syu jupyter-notebook 
pacman -Syu  notepadqq 
pacman -Syu  pycharm-community-edition 
pacman -Syu code 
pacman -Syu simplescreenrecorder
pacman -Syu pamac-gtk
pacman -Syu git base-devel 
pacman -Syu git
pacman -Syu docker
pacman -Syu docker-compose
pacman -Syu pambase ## Hay algunos temas con el login
pacman -Syy meld
pacman -S texlive-most ## Latex
```

## Yay + Slack

```
git clone https://aur.archlinux.org/slack-desktop.git
git clone https://aur.archlinux.org/yay.git
exit
chown -R lucas ./
cd slack-desktop
makepkg -sri
cd ..
cd yay
makepkg -si
yay -S slack-desktop
yay -S google-chrome

```

## Some snap installs 

```
snap install drawio
snap install qbittorrent-arnatious
snap install postman
snap install google-chat-electron

```

## Vs code

Ref: https://linuxhint.com/install_visual_studio_code_archlinux/

```
curl -L -O https://aur.archlinux.org/cgit/aur.git/snapshot/visual-studio-code-bin.tar.gz
tar -xvf visual-studio-code-bin.tar.gz
cd visual-studio-code-bin
makepkg -si
code & ## Lo abre
```

## Docker

```
sudo groupadd docker
sudo usermod -aG docker ${USER}
#Cannot connect to the Docker daemon at unix:/var/run/docker.sock. Is the docker daemon running?
## Reiniciarlo: sudo systemctl restart docker
## Iniciarlo: 
## 	sudo dockerd
## 	systemctl start docker
## Configurar inicio en el inicio de linux:
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
## https://stackoverflow.com/questions/47854463/docker-got-permission-denied-while-trying-to-connect-to-the-docker-daemon-socke
## Permisos al grupo docker
sudo usermod -a -G docker lucas
## Para validar. Output esperado: "docker:x:998:lucas"
grep docker /etc/group
## Test
docker run hello-world
```

## Portainer para administrarlo

```
mkdir ~/portainer
docker pull portainer/portainer
docker tag portainer/portainer portainer
export CONT_NAME="portainer"
docker run -d -p 9000:9000 --restart always -v /var/run/docker.sock:/var/run/docker.sock -v ~/portainer:/data --name ${CONT_NAME} portainer
## http://127.0.0.1 ## LibreriaMundial
chrome http://127.0.0.1:9000/#/home
```

## update all packages
```
pacman -Syu $(pacman -Qq)
yay -Syu
snap refresh
```

## Default java 15
```
sudo archlinux-java set java-15-openjdk
## Ver status, que quede seleccionado
archlinux-java status
```

## Node
```
## Ref: https://wiki.archlinux.org/index.php/Node.js
sudo pacman -Syy nodejs-lts-dubnium
sudo pacman -Syy npm
curl -o- -L https://yarnpkg.com/install.sh | bash
```

## git config
```
git config --global user.name "blukitas"
git config --global user.email "lucasbertolini@hotmail.com"
git config --global core.editor "notepadqq"
```
